Assign macro a, b
    mov ax, [b]
    mov [a], ax    
endm


Negate macro a
    mov ax, [a]
    neg ax
    mov [a], ax    
endm

IncVar macro a
    mov ax, [a]
    inc ax
    mov [a], ax    
endm

DecVar macro a
    mov ax, [a]
    dec ax
    mov [a], ax    
endm

Compare2Variables macro a, b
    mov cx, [a]
    cmp cx, [b]
endm

CompareVariableAndNumber macro a, b
    mov cx, [a]
    cmp cx, b
endm

AddAndAssign macro c, a, b
    mov ax, [a]
    add ax, [b]
    mov [c], ax
endm 

SubAndAssign macro c, a, b
    mov ax, [a]
    sub ax, [b]
    mov [c], ax
endm

Add3NumbersAndAssign macro d, a, b, c
    mov ax, [a]
    add ax, [b]
    add ax, [c]
    mov [d], ax
endm 

Sub3NumbersAndAssign macro d, a, b, c
    mov ax, [a]
    sub ax, [b]
    sub ax, [c]
    mov [d], ax
endm

DrawPixel macro x, y
    
    mov cx, [x]   
    mov dx, [y]  
     
    mov al, 10  
    mov ah, 0ch 
    int 10h     
endm

DrawCircle macro circleCenterX, circleCenterY, radius

    balance dw 0
    xoff dw 0
    yoff dw 0 
    
    xplusx dw 0
    xminusx dw 0
    yplusy dw 0
    yminusy dw 0
    
    xplusy dw 0
    xminusy dw 0
    yplusx dw 0
    yminusx dw 0
    
    Assign yoff, radius
    
    Assign balance, radius
    Negate balance
    
    draw_circle_loop:
     
     AddAndAssign xplusx, circleCenterX, xoff
     SubAndAssign xminusx, circleCenterX, xoff
     AddAndAssign yplusy, circleCenterY, yoff
     SubAndAssign yminusy, circleCenterY, yoff
     
     AddAndAssign xplusy, circleCenterX, yoff
     SubAndAssign xminusy, circleCenterX, yoff
     AddAndAssign yplusx, circleCenterY, xoff
     SubAndAssign yminusx, circleCenterY, xoff
   
    DrawPixel xplusy, yminusx
  
    DrawPixel xplusx, yminusy
      
    DrawPixel xminusx, yminusy
       
    DrawPixel xminusy, yminusx
       
    DrawPixel xminusy, yplusx
      
    DrawPixel xminusx, yplusy
           
    DrawPixel xplusx, yplusy
        
    DrawPixel xplusy, yplusx
   
    Add3NumbersAndAssign balance, balance, xoff, xoff
       
    CompareVariableAndNumber balance, 0
    jl balance_negative
   
    DecVar yoff
    
    Sub3NumbersAndAssign balance, balance, yoff, yoff
    
    balance_negative:
    IncVar xoff
    
    Compare2Variables xoff, yoff
    jg end_drawing
    jmp draw_circle_loop
    
    end_drawing:
        
endm

org  100h

mov ah, 0   
mov al, 13h 
int 10h     
         
x dw 80 
y dw 80 
r dw 20 

DrawCircle x, y, r

  mov ah,00
  int 16h			

  mov ah,00 
  mov al,03 
  int 10h   

ret
